# freebsd.kde.org

This is the Website of KDE FreeBSD initiative

Build instruction

```
gem install bundler jekyll --user-install
bundle install --path vendor/bundle
```

## Run development

```
bundle exec jekyll serve
```

## Run production

```
bundle exec jekyll build
```
