--- 
title: "People behind KDE/FreeBSD"
layout: page
---

# People behind KDE/FreeBSD

The KDE/FreeBSD ports are maintained by a very loosely organized team of volunteers, which can be reached at the general e-mail address [kde@FreeBSD.org](mailto:kde@FreeBSD.org).
The team can be reached (most awake during European daylight hours)
on freenode as well, in [#kde-freebsd](http://webchat.freenode.net/?channels=%23kde-freebsd).

## The maintainers

The maintainers deal with the day-to-day maintenance and longterm development of the KDE ports in the FreeBSD ports collection, handle problem reports in FreeBSD's bug tracking system, provide technical support to end-users and work with the contributors below to keep KDE Software Compilation working on FreeBSD as smoothly as possible.

<table width="100%">
	<tr>
		<td>
			<ul>
				<li>Tobias C. Berner (tcberner@)</li>
				<li>Adriaan de Groot (adridg@)</li>
				<li>Jason E. Hale (jhale@)</li>
				<li>Raphael Kubo da Costa (rakuco@)</li>
				<li>Gleb Popov (arrowd@)</li>
			</ul>
		</td>
	</tr>
</table>


## Contributors/developers

The contributors/developers invest time, efforts, knowledge and resources into the active maintenance of KDE Software Compilation on FreeBSD. They work on the KDE SC codebase, do beta testing, provide technical support to end-users and maintain various ports in the FreeBSD ports collection.

<table width="100%">
	<tr>
		<td width="33%">
			<ul>
				<li>Andy Fawcett</li>
				<li>Kris Moore</li>
				<li>Josh Paetzel</li>
				<li>Bartosz Fabianowski</li>
				<li>David Bruce Naylor</li>
				<li>Marie Loise Nolden</li>
			</ul>
		</td>
		<td width="33%">
			<ul>
				<li>David Johnson</li>
				<li>Mikhail Teterin</li>
				<li>Andriy Gapon</li>
				<li>Tijl Coosemans</li>
				<li>Dima Panov</li>
				<li>Alonso Schaich</li>
			</ul>
		</td>
		<td width="33%">
			<ul>
				<li>Axel Gonzalez</li>
				<li>Li-Wen Hsu</li>
				<li>Matthew Rezny</li>
				<li>Max Brazhnikov</li>
				<li>Alberto Villa</li>
				<li>Martin Wilke</li>
			</ul>
		</td>
	</tr>
</table>


...and many more.

## Alumni

Although they have since moved on to other projects, without the outstanding efforts of these people, there would be no KDE/FreeBSD as we know it today.

<table width="100%">
	<tr>
		<td width="33%">
			<ul>
				<li>Satoshi Asami</li>
				<li>Stefan Esser</li>
				<li>Kevin Lo</li>
				<li>Dimitry Sivachenko</li>
				<li>Lauri Watts</li>
				<li>Wes Morgan</li>
				<li>Will Andrews</li>
				<li>Matt Douhan</li>
				<li>Jonathan Drews</li>
				<li>Pete Fritchman</li>
			</ul>
		</td>
		<td width="33%">
			<ul>
				<li>Frank Laszlo</li>
				<li><a href="memoriam/alane.php">Alan Eldridge</a></li>
				<li>M&aacute;rio S&eacute;rgio Fujikawa Ferreira</li>
				<li>Mike O'Brien</li>
				<li>Rob Kaper</li>
				<li>Kaarthik Sivakumar</li>
				<li>Michael L. Hostbaek</li>
				<li>Brad Davis</li>
				<li>Michael Nottebrock</li>
			</ul>
		</td>
		<td width="33%">
			<ul>
				<li>Puneet Madaan</li>
				<li>Manolo Valdes</li>
				<li>Daniel W. Steinbrook</li>
				<li>Chris Howells</li>
				<li>Arjan van Leeuwen</li>
				<li>Philip Rodrigues</li>
				<li>Danny Pansters</li>
				<li>Tilman Linneweh</li>
				<li>Markus Brueffer</li>
				<li>Thomas Abthorpe</li>
			</ul>
		</td>
	</tr>
</table>
