---
title: 'CFT: Qt 4.4.2 released'
date: 2008-09-19 00:00:00 
layout: post
---

<p>Yesterday Qt 4.4.2 was released. area51 is already updated, and I would like some testers before we commit it to the ports tree after the freeze.</p><p>You can find the changelog here:<br />
			<a href="http://trolltech.com/developer/resources/notes/changes/changes-4.4.2">http://trolltech.com/developer/resources/notes/changes/changes-4.4.2</a>.</p><p>Note on area51:<br />
			you now need Subversion to checkout area51:</p><pre>
$ svn co https://kf.athame.co.uk/kde-FreeBSD/trunk/area51
</pre><p>Happy testing!</p>