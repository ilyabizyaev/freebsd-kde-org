---
title: 'KDE 4.2.3 in ports'
date: 2009-05-09 00:00:00 
layout: post
---

<p>KDE 4.2.3 has been committed to ports.</p><p>For more information see original <a href="http://kde.org/announcements/announce-4.2.3.php">announcement</a> and <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php">changelog</a>.</p><p>New ports:</p><pre>
devel/kdebindings4:
    Meta port of KDE bindings for C#, Java, PHP, Python and Ruby. Currently only
    Python bindings are supported.

devel/kdebindings4-python, devel/kdebindings4-python-krosspython,
devel/kdebindings4-python-pykde4:
    Python bindings for KDE.

print/kdeutils4-printer-applet:
    System tray utility which shows current print jobs, printer warnings and
    errors and shows when printers plugged in for the first time are being
    auto-configured by hal-cups-utils. It replaces kjobviewer in KDE 3.

print/system-config-printer-kde
    A port of GNOME system-config-printer to KDE.
</pre><p>Special thanks for help and feedback to Dima Panov, Matt Tosto, Kris Moore and Josh Paetzel.</p>