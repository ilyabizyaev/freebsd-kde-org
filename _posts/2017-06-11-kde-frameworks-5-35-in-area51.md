---
title: "KDE Frameworks 5.35 in Area51"
date: 2017-06-11 00:00:00
layout: post
---

      The latest release of KDE Frameworks 5.35 has been
      added to the <code>plasma5/</code> branch of the
      [Area51 repository](https://github.com/freebsd/freebsd-ports-kde).
      Users of the experimental ports repository can upate and
      rebuild KDE Frameworks as usual.
