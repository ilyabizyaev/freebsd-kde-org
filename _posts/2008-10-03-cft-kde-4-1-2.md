---
title: 'CFT: KDE 4.1.2'
date: 2008-10-03 00:00:00 
layout: post
---

<p>The KDE team released KDE 4.1.2 two days later than planned. We are in ports slush and can't update KDE until it has ended. If you can't wait to get KDE 4.1.2, you can now download it from our area51 repo. The full KDE changelog can be seen here:<br />
			<a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>.</p><p>We removed FAM support completely. This gives a bit more speed and the programs start faster. Thanks to Kris Moore (PC-BSD) who tested this patchset, we also removed the KDE debug modules.</p><p>Also, Hannes Hauswedell <a href="http://mail.kde.org/pipermail/kde-FreeBSD/2008-September/003893.html">reported a tweak</a>. He experienced high CPU usage by kded4 and slowdowns with Konqueror as file browser, so he recommends to radically lower the DirWatch rate of kded4 by adding the following to your $HOME/.kde4/share/config/kdedrc:<br /></p><pre>
[DirWatch]
PollInterval=60000
</pre><p>This is explained <a href="https://bugs.kde.org/show_bug.cgi?id=155904">here</a>.</p><p>So, if you want to get the ports, please read <a href="https://kf.athame.co.uk/area51.php">these instructions</a> (note: you first need to update Qt to 4.4.2).</p>