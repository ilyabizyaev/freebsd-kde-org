---
title: 'KDE SC 4.6.4 releasd'
date: 2011-06-14 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is pleased to announce KDE SC 4.6.4. Read the full announcement <a href="http://kde.org/announcements/announce-4.6.4.php">here</a>.</p>