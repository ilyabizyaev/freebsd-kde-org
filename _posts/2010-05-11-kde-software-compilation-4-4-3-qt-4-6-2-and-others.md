---
title: 'KDE Software Compilation 4.4.3, Qt 4.6.2 and others'
date: 2010-05-11 00:00:00 
layout: post
---

<p>After a long delay due to ports freeze in preparation for <a href="http://www.FreeBSD.org/releases/7.3R/announce.html">FreeBSD 7.3 release</a>, and then subsequent updates for X.Org and <a href="http://www.FreeBSD.org/gnome/newsflash.html#event2010May10:0">GNOME ports</a>, the eagerly awaited <a href="http://www.kde.org/announcements/announce-4.4.3.php">KDE SC 4.4.3</a> with <a href="http://qt.nokia.com/developer/changes/changes-4.6.2">Qt 4.6.2</a> has been committed to ports! Besides, KOffice ports have been updated to 2.1.2 and PyQt4 to 4.7.3. We had to resurrect multimedia/phonon* ports and now they are installed by default instead of Qt 4 Phonon.</p><p>Please, read carefully /usr/ports/UPDATING before upgrading your ports!</p><p>New ports:</p><pre>
accessibility/ktts (split from accessibility/kdeaccessibility):
    KDE subsystem for conversion of text to audible speech.

devel/kdebindings4-ruby:
    Ruby bindings for KDE.

x11/plasma-scriptengine-python (split from x11/kdebase4-workspace):
    Allows to run Python plasmoids.

x11/plasma-scriptengine-ruby:
    Allows to run Ruby plasmoids.

www/kwebkitpart:
    A web browser component for KDE (KPart).
</pre>