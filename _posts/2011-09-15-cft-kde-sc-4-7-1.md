---
title: 'CFT: KDE SC 4.7.1'
date: 2011-09-15 00:00:00 
layout: post
---

<p>We're happy to let you know that KDE SC 4.7.1 is ready for a public test.</p><p>You can get it via SVN checkout:</p><pre>
$ svn co http://area51.pcbsd.org/trunk/area51
# sh Tools/scripts/kdemerge -a /usr/ports
</pre><p>If you want to also test KDE PIM 4.7.1, run (at this exact point):</p><pre>
# sh Tools/scripts/kdemerge -m /usr/ports
</pre><p>Alternatively, if you're on amd64, you can use precompiled packages:</p><pre>
# PACKAGEROOT=http://packages.FreeBSD.kde.org portmaster -P x11/kde4
</pre><p><b>Warning:</b> if you&#8217;re using a recent -CURRENT (after libraries soversion bump), you'll need to adjust your <tt><a href="http://www.freebsd.org/cgi/man.cgi?query=libmap.conf&amp;apropos=0&amp;sektion=0&amp;manpath=FreeBSD+9-current&amp;arch=default&amp;format=html">libmap.conf</a></tt> to use those packages.</p><p>Please read carefully the <tt><a href="http://area51.pcbsd.org/trunk/area51/UPDATING-area51">UPDATING-area51</a></tt> notes.</p><p>We'd like to say thanks to Li-Wen Hsu (lwhsu@) for hosting and syncing the packages.</p>