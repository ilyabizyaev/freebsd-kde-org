---
title: 'KDE Frameworks 5.23.0 in area51'
date: 2016-06-13 00:00:00 
layout: post
---

<p>
The unofficial ports repository <a href="https://freebsd.kde.org/area51.php">area51</a> has been updated with KDE Frameworks 5.23.0 which were released today. The plasma5-branch now holds the latest Frameworks, and Plasma 5.6.4 and KDE Applications 16.04.1.
    </p>