---
title: 'KDE 3.5.7 in ports'
date: 2007-07-04 00:00:00 
layout: post
---

<p>KDE 3.5.7 has been committed to ports.</p><p>UPDATING:</p><pre>
20070704:
  AFFECTS: users of x11/kdebase3
  AUTHOR: kde@FreeBSD.org

  The media kioslave now mounts all filesystems that support charset conversion
  with charset conversion turned on, set to the locale from the user's
  environment (LANG). If you are using a customized kernel configuration, make
  sure all *_iconv kernel modules are available. At the time of writing, these
  are cd9660_iconv, udf_iconv, msdosfs_iconv and ntfs_iconv.
</pre><p>For more information about KDE 3.5.7, see the KDE 3.5.7 <a href="http://www.kde.org/info/3.5.7.php">info page</a>.</p>