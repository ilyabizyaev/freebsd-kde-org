---
title: "KDE Frameworks 5.49 Landed"
date: 2018-08-23 00:00:00
layout: post
---

      The latest monthly version of KDE Frameworks is available
      in the official FreeBSD ports tree.
