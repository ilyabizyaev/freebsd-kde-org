---
title: 'Welcome KDE SC 4.14.2'
date: 2014-11-19 00:00:00 
layout: post
---

<p>The KDE Community proudly announces the latest release of Plasma Workspaces 4.11.13 and <a href="https://www.kde.org/announcements/announce-4.14.2.php">KDE Applications and Platform 4.14.2</a>.</p>