---
title: 'Frameworks 5.17 in area51'
date: 2015-12-13 00:00:00 
layout: post
---

<p>Just a day after their <a href="https://www.kde.org/announcements/kde-frameworks-5.17.0.php" title="Frameworks 5.17.0 announcement">release</a> by the KDE community, the KDE Frameworks 5 ports have been updated to the latest version, KDE Frameworks 5.17.0. These ports are intended for testing before eventually entering the official ports tree. These new ports are available in <a href="area51.php">area51</a> in the <i>plasma5</i> branch.
    </p>