---
title: "Welcome Modern KDE to FreeBSD"
date: 2018-05-03 00:00:00
layout: post
---

      The long-, long-awaited up-to-date KDE Plasma Desktop
      and all of the KDE Applications have landed in the
      official ports tree.
      The <tt>x11/kde5</tt> metaport installs KDE Frameworks,
      KDE Plasma Desktop, and KDE Applications all of those
      are metaports as well, do take a look at the configuration
      options to get the applications you want.
