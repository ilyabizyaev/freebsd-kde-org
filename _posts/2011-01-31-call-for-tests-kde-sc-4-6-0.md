---
title: 'Call for tests: KDE SC 4.6.0'
date: 2011-01-31 00:00:00 
layout: post
---

<p>The FreeBSD KDE Team is happy to let you know that KDE SC 4.6.0 has been released a few days ago, and the release is ready for a public test. Before you ask, no, we do not want to put KDE 4.6.0 in the ports tree before FreeBSD 8.2/7.4 is released.</p><p><b>What's new:</b><br />
			KDE SC 4.6.0 provides major updates to the KDE Plasma workspaces, KDE Applications and KDE Platform. Theses releases, version 4.6, provide many new features in each of KDE's three product lines. The official release notes for these releases can be found at <a href="http://kde.org/announcements/4.6">http://kde.org/announcements/4.6</a>.</p><p>You can get KDE SC 4.6 via SVN checkout:</p><pre>
$ svn co http://area51.pcbsd.org/trunk/area51/PORTS
$ svn co http://area51.pcbsd.org/trunk/area51/PYQT
$ svn co http://area51.pcbsd.org/trunk/area51/KDE
$ svn co http://area51.pcbsd.org/trunk/area51/Tools
</pre><p>Then run:</p><pre>
# sh Tools/scripts/kdemerge -kmp /usr/ports
</pre><p>Please read carefully the <a href="http://area51.pcbsd.org/trunk/area51/UPDATING-area51">UPDATING-area51</a> notes.</p><p>Happy updating!!</p>