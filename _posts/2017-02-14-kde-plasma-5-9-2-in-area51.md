---
title: "KDE Plasma 5.9.2 in area51"
date: 2017-02-14 00:00:00
layout: post
---

    The KDE-FreeBSD team loves announcing that area51 is all up-to-datewith the latest software releases from the KDE community.The [unofficial ports tree](https://freebsd.kde.org/area51.php)
    for KDE-FreeBSD now contains [KDE Frameworks 5.31](https://www.kde.org/announcements/kde-frameworks-5.31.0.php),
    [KDE Plasma Desktop 5.9.2](https://www.kde.org/announcements/plasma-5.9.2.php) and
    [KDE Applications 16.12](https://www.kde.org/announcements/announce-applications-16.12.2.php)
