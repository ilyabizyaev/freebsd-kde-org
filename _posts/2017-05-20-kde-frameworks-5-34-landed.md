---
title: "KDE Frameworks 5.34 landed"
date: 2017-05-20 00:00:00
layout: post
---

    The official ports for KDE Frameworks 5 have been updated to the [latest release](https://www.kde.org/announcements/kde-frameworks-5.34.0.php). Users of KDE Frameworks ports can update as usual.
