---
title: 'KDE Plasma 5.6.5 in area51'
date: 2016-06-13 00:00:00 
layout: post
---

<p>
The unofficial ports repository <a href="https://freebsd.kde.org/area51.php">area51</a> has been updated with the latest release of <a href="https://www.kde.org/announcements/plasma-5.6.5.php">KDE Plasma, 5.6.5</a>. Users of the unofficial repository can simply build new packages with poudriere. There is no estimate yet on when these ports will end up in the official ports tree.
    </p>