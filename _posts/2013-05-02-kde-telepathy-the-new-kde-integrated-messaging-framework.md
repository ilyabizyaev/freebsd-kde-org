---
title: 'KDE Telepathy: the new KDE integrated messaging framework'
date: 2013-05-02 00:00:00 
layout: post
---

<p>Please welcome <b>KDE Telepathy</b> 0.6.1 (<tt><a href="http://freshports.org/net-im/kde-telepathy">net-im/kde-telepathy</a></tt>), the new KDE project aiming to integrate real time communication deeply into KDE Workspaces. You can find a comprehensive introduction on <a href="http://dot.kde.org/2013/04/09/new-kde-telepathy-brings-better-text-editing-and-improved-notifications">dot.kde.org</a>.</p>