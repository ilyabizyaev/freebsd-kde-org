---
title: 'KDE Applications 16.08.0 in area51'
date: 2016-08-19 00:00:00 
layout: post
---

<p>
The unofficial ports tree for KDE-FreeBSD, <a href="https://freebsd.kde.org/area51.php">area51</a>,
has been updated with the newest release of
<a href="https://www.kde.org/announcements/kde-frameworks-5.25.0.php">KDE Frameworks</a>, version 5.25, and with the latest release of
<a href="https://www.kde.org/announcements/announce-applications-16.08.0.php">KDE Applications</a>, 16.08.0.
Users of the plasma5 branch of the area51 repository can update normally.
    </p>