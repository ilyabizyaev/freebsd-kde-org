---
title: "KDE Frameworks 5.30 landed"
date: 2017-01-26 00:00:00
layout: post
---

    The official FreeBSD ports tree has been updated with this month's
    KDE Frameworks release, [version 5.30](https://www.kde.org/announcements/kde-frameworks-5.30.0.php). New in this release on
    the FreeBSD side is the inclusion of *x11/kf5-kwayland*, which
    opens the door to Wayland-compatible applications using the
    KDE Frameworks.
