---
title: 'KDE 4.14.9 in area51'
date: 2016-03-04 00:00:00 
layout: post
---

<p>
The KDE4 ports in <a href="area51.php">area51</a> have been updated
to KDE4 4.14.9. This does not match an official release, but updates kdelibs to the most recent released versions. Most KDE ports do not update and remain 4.14.3.
<a href="https://www.kdevelop.org/news/kdevelop-473-release">KDevelop</a> has been updated to the latest version based on KDE4 libraries.
Users can update the trunk branch in the unofficial repository
to try these newer ports. Work is underway to merge this update with
the official ports tree.
    </p>