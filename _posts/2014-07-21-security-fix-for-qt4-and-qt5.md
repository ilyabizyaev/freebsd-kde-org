---
title: 'Security fix for Qt4 and Qt5'
date: 2014-07-21 00:00:00 
layout: post
---

<p>A fix for <a href="http://www.vuxml.org/freebsd/CVE-2014-0190.html">CVE-2014-0190</a> has been committed to the Qt4 and Qt5 ports. Users are advised to rebuild qt4-imageformats and qt5-gui.</p>