---
title: 'KDE 3.5.9 now available'
date: 2008-08-18 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is proud to announce the release of KDE 3.5.9 for FreeBSD. The official KDE 3.5.9 release notes can be found at:<br />
			<a href="http://www.kde.org/announcements/announce-3.5.9.php">http://www.kde.org/announcements/announce-3.5.9.php</a>.</p><p>After the KDE 4.1.0 import the team found time to make KDE 3.5.9 ready for the ports tree.</p><p>KDE 3.5.9 was released six months ago and 3.5.10 is coming soon. We are not sure we have time to get 3.5.10 for FreeBSD 7.1/6.4 release, but we would have minimum 3.5.9 for those people who prefer to stay with KDE 3.</p>